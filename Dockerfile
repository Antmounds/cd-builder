FROM alpine:latest

ENV PATH=$PATH:/root/.local/bin

ARG TERRAFORM_VERSION=0.11.7

# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
RUN apk -v --update --no-cache --virtual add \
	bash \
	git \
	python \
	py-pip \
	openrc \
	openssh \
	docker; \
	pip install --upgrade pip && \
	pip install pip awscli docker-compose --upgrade --user && \
	rm /var/cache/apk/* && \

	# Terraform
	wget -q https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
	unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
	rm terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
	mv terraform /usr/local/bin && \

	#wget https://github.com/docker/compose/releases/download/1.21.2/run.sh -O /usr/local/bin/docker-compose && \
	#chmod +x /usr/local/bin/docker-compose && \

	apk -v info && \

	# Start Docker on boot
	rc-update add docker boot

VOLUME /root/.aws
ENTRYPOINT /bin/sh